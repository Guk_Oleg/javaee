package ua.lviv.javaEE.hibernateAnotation;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import ua.lviv.javaEE.hibernateAnotation.entity.User;


public class Main {

    public static void main(String[] args) {
        SessionFactory factory = new Configuration().configure().buildSessionFactory();
        Session session = factory.openSession();
        User user = new User();
        user.setName("Oleg");
        user.setAge(26);
        session.beginTransaction();
        session.persist(user);

            session.getTransaction().commit();
        }

    }