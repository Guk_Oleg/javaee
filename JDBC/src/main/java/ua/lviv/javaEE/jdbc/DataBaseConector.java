package ua.lviv.javaEE.jdbc;

import java.sql.*;

public class DataBaseConector {

    public static final String URL = "jdbc:mysql://localhost:3306/test_db?useSSL=false";
    public static final String USER = "root";
    public static final String PASSWORD = "root";

    private static Connection connection;
    private static Statement statement;
    private static ResultSet resultSet;
    private static PreparedStatement preparedStatement;

    public static void main(String[] args) {
        String name1 = "Zenoviy";
        String name2 = "Ivan";
        int age1 = 25;
        String insert1 = "insert into user(name, age) values(" + "'" + name1 + "'" + ", 20)";
        String insert2 = "insert into user(name, age) values('Tanya', 28)";
        String insert3 = "insert into user(name, age) values('Olya', 15)";

        String delete = "delete from user where name = 'Olya'";

        String update = "UPDATE user set name = 'Victor' where id = 4";

        String select = "Select * from user";


        try {
            connection = DriverManager.getConnection(URL, USER, PASSWORD);
            statement = connection.createStatement();
            preparedStatement = connection.prepareStatement("insert into user(name, age) values(?, ?)");

            preparedStatement.setString(1,name2);
            preparedStatement.setInt(2,age1);

            preparedStatement.execute();
            //statement.execute(insert1);
            resultSet = statement.getResultSet();
            resultSet = statement.executeQuery(select);
            while (resultSet.next()) {
                String name = resultSet.getString("name");
                int age = resultSet.getInt("age");
                System.out.println(name + " " + age);
            }


            statement.close();
            connection.close();
        } catch (SQLException e) {
            System.err.println("Conection error");
            e.printStackTrace();
        }


    }

}
