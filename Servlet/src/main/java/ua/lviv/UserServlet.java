package ua.lviv;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/user")
public class UserServlet extends HttpServlet {

    private UserDB userDB;
    List<String> stringList = new ArrayList<>();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException  {

        stringList.add("AAAA");
        stringList.add("BBB");
        stringList.add("CCCC");

        UserDB userDB = new UserDB();
        List<User> userList = userDB.getUserList();
        /*logger.info("before:    ");
        req.setAttribute("listString", stringList);
        logger.info("after:    ");*/
        PrintWriter printWriter = resp.getWriter();
        /*for (User user: userList) {
            printWriter.println(user);
            System.out.println(user);
        }*/
        printWriter.println(userList.toString());
        printWriter.close();

/*
        req.getRequestDispatcher("user.jsp").forward(req, resp);
*/
    }
}
