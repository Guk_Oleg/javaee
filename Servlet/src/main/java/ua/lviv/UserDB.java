package ua.lviv;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by User on 2/3/2017.
 */
public class UserDB {
    private final String url = "jdbc:mysql://localhost:3306/test_db?useSSL=true";
    private final String user = "root";
    private final String password = "root";

    private Connection connection;
    private PreparedStatement preparedStatement;
    private Statement statement;
    private ResultSet resultSet;


    public void addUser(String nameOfUser, Integer ageOfUser){
        try {
            connection = DriverManager.getConnection(url, user, password);
            preparedStatement = connection.prepareStatement("insert into user(name, age) values(?, ?)");
            preparedStatement.setString(1, nameOfUser);
            preparedStatement.setInt(2, ageOfUser);
            preparedStatement.execute();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


    public List<User> getUserList(){
        List<User> userList = new ArrayList<User>();
        try {
            connection = DriverManager.getConnection(url, user, password);
            statement = connection.createStatement();
            resultSet = statement.executeQuery("select * from user");
            while (resultSet.next()){
                User user = new User();
                user.setId(resultSet.getInt("id"));
                user.setName(resultSet.getString("name"));
                user.setAge(resultSet.getInt("age"));
                userList.add(user);
            }
            resultSet.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userList;
    }



    public static void main(String[] args) {
/*        System.out.println(new UserDB().getUserList());
        UserDB userDB = new UserDB();
        userDB.addUser("Ivan", 25);
        userDB.addUser("Ivanka", 25);
        userDB.addUser("Tanya", 28);
        userDB.addUser("Arina", 32);*/
        System.out.println(new UserDB().getUserList());
    }

}
