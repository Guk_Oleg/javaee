package ua.javaEE.springIoC.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ua.javaEE.springIoC.entity.User;

@Configuration
public class SpringConfig {

    @Bean(name = "user")
    public User getUser(){
        return new User();
    }

    @Bean(name = "user2")
    public User getUser2(){
        User user = new User();
        user.setId(2);
        user.setAge(25);
        user.setUserName("Oleg");
        return user;
    }

    @Bean(name = "user3")
    public User getUser3(){
        return new User(3, "Galya", 96);
    }
}
