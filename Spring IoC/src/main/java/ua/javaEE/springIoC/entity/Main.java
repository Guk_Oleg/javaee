package ua.javaEE.springIoC.entity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ua.javaEE.springIoC.config.SpringConfig;

public class Main {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("SpringConfig.xml");
        User user1 = (User) context.getBean("User4");
        System.out.println(user1.toString());

        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(SpringConfig.class);
        User user = (User) applicationContext.getBean("user2");
        System.out.println(user.toString());
    }
}
