import org.hibernate.Session;
import ua.lviv.javaEE.hibernate.config.HibernateUtil;
import ua.lviv.javaEE.hibernate.entity.User;

public class Main {

    public static void main(String[] args) {
        Session session = (Session) HibernateUtil.getSessionFactory();
        session.beginTransaction();
        User user = new User();
        user.setAge(25);
        user.setName("Ivan");
        session.save(user);
        if (session.getTransaction()!= null){
            session.getTransaction().rollback();
        }else {
            session.getTransaction().commit();
        }

    }
}